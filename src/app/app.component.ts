import { Component, OnInit, ChangeDetectionStrategy, OnDestroy } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Store, select } from '@ngrx/store';
import { leadSetPreviousStep, leadPatchSingleValue, leadPatchStep, leadGetLead } from './store/lead/actions';
import { selectLeadStepIndex } from './store/lead/selectors';
import { distinctUntilChanged, tap, mergeAll, subscribeOn } from 'rxjs/operators';
import { LeadService } from './services/lead.service';
import { merge, Observable } from 'rxjs';
import { ValidatorsService } from './services/validators.service';
import { WebSocketService } from './services/web-socket.service';
import { testConnectWebSocket } from './store/test/actions';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent implements OnInit, OnDestroy {

  currentStepIndex = 0;

  // Step 1: Contact
  contactForm = this.fb.group(
    {
      first_name: ['', Validators.required],
      last_name: ['', Validators.required],
      street_address: ['', Validators.required],
      city: ['', Validators.required],
      state: ['', Validators.required],
      zip: ['', Validators.required]
    },
    {
      updateOn: 'blur',
      // Bound to service since we need to reference the stateZipMap
      validators: this.validatorsService.stateZipMismatch.bind(this.validatorsService)
    });

  // Step 2: Personal
  personalForm = this.fb.group({
    dob: ['', Validators.required],
    main_phone: ['', [
      Validators.required,
      this.validatorsService.phoneRegex()
    ]],
    second_phone: ['', [
      this.validatorsService.phoneRegex()
    ]],
    ssn: ['', [
      Validators.required,
      this.validatorsService.ssnRegex()
    ]], // not saved in store
    annual_income: ['', [
      Validators.required,
    ], [
      this.validatorsService.incomeLowForStateOfUtah.bind(this.validatorsService)
    ]],
  },
  {
    updateOn: 'blur',
    validators: [
      this.validatorsService.incomeLowForDob
    ]
  } );

  // Step 3: Employment
  employmentForm = this.fb.group({
    employment_status: ['', Validators.required],
    length_of_employment: ['', Validators.required],
  }, { updateOn: 'blur' } );

  // Step 4: Housing
  housingForm = this.fb.group({
    housing_type: ['', Validators.required],
    housing_payment: ['', Validators.required],
  }, { updateOn: 'blur' } );

  // Step 5: Banking
  bankingForm = this.fb.group({
    bank_accounts: [''],
    // These fields wouldn't be sent to backend
    has_checking: [''],
    has_savings: [''],
    has_retirement: [''],
    has_investments: [''],
  }, { updateOn: 'blur' } );

  // Step 6: Terms
  termsForm = this.fb.group({
    all_terms_agree: ['', Validators.requiredTrue],
  }, { updateOn: 'blur' } );

  /**
   * Represents each funnel step. There'd be a separate component
   * for each step.
   * Simple strings for now.
  */
 steps = [
    {name: 'contact', form: this.contactForm},
    {name: 'personal', form: this.personalForm},
    {name: 'employment', form: this.employmentForm},
    {name: 'housing', form: this.housingForm},
    {name: 'banking', form: this.bankingForm},
    {name: 'terms', form: this.termsForm}
  ];

  /**
   * Testing BroadcastChannel API
  */
 bc;

  constructor(
    private fb: FormBuilder,
    private store: Store<any>,
    private leadService: LeadService,
    private validatorsService: ValidatorsService,
    private webSocketService: WebSocketService
  ){}

  ngOnInit() {
    // Listen to changes in funnel step
    this.store.pipe(select(selectLeadStepIndex))
      .subscribe(res => {
        // console.log('selectLeadStepIndex res', res);
        this.currentStepIndex = res;
        // console.log(this.currentStepName, this.currentForm);

        // testing GET lead
        // if (this.currentStepIndex === 3) {
        //   this.store.dispatch(leadGetLead({ id: 1234 }));
        // }

      });

    // Try creating a new lead right here
    // this.leadService.createLead()
    //   .pipe(
    //     tap(res => {
    //       console.log('res', res);
    //     })
    //   )
    //   .subscribe();

    this.store.dispatch(leadGetLead({ id: 1234 }));

    // this.leadService.getLeads()
    //   .subscribe(res => console.log('getLeads res', res));



    // if we wanted to subscribe to distinct value changes on each form and emit the event with action as payload
    // (just trying contactForm for now)
    // this.contactForm.valueChanges.pipe(distinctUntilChanged()).subscribe((value) => {
    //   this.store.dispatch(leadPatchSingleValue({ payload: value }));
    // });



    // Just for testing (for now)
    const allValueChanges = this.steps
      .map(step =>
        step.form.valueChanges.pipe(distinctUntilChanged())
      );
    merge(...allValueChanges)
      .subscribe((value) => {
        // console.log('value', value);
        // console.log(this.currentStepName, this.currentForm);

        // testing websocket by sending to the socket on every value change
        // this.webSocketService.send({
        //   message: value,
        //   id: 1234
        // });
      });

    // broadcast testing
    // this.broadcastChannelInit();

    // localStorage testing
    // let lsCount = 0;

    // const localStorage$ = this.localStorageListen();

    // let lsSubscription = localStorage$
    //   .subscribe((res) => {
    //     console.log('from ls sub', res);
    //     lsCount++;

    //     if (lsCount > 5) {
    //       // debugger;
    //       console.log('should unsub');
    //       lsSubscription.unsubscribe();
    //     }
    //   });

  }

  /**
   * Fires when proceeding through each step. The logic would
   * be the same here:
   * - We disable submitting unless front end validation passes.
   * - We dispatch the action/effect to fire the PATCH request
   * - On success, we proceed to the next step.
   * - On backend errors, we display them accordingly.
  */
  submit() {
    // console.log(this.currentForm);
    this.store.dispatch(leadPatchStep({ payload: this.currentForm.value }));
  }

  goToPreviousStep() {
    this.store.dispatch(leadSetPreviousStep());
  }

  /**
   * Testing websocket
  */
  connectWebSocket(){
    // Connect via NgRx effect
    this.store.dispatch(testConnectWebSocket({}));

    // Non-NgRx way
    // this.webSocketService.connect('ws://echo.websocket.org/')
    //   .subscribe(res => {
    //     console.log('subscribing to connection$, res', res);
    //   });
  }

  disconnectWebSocket(){
    this.webSocketService.closeConnection();
  }


  /**
   * Testing BroadcastChannelAPI
  */
  broadcastChannelInit() {
    // Create the channel
    this.bc = new BroadcastChannel('test_channel');

    // Listen for messages
    this.bc.onmessage = function (ev) { console.log(ev); }
  }

  // Broadcast message
  broadcastChannelPost() {
    console.log('broadcastChannelPost');
    this.bc.postMessage('This is a test message.');
  }


  /**
   * Testing localStorage API
  */
  localStorageListen() {

    let storageListener;

    return Observable.create(function(observer) {
      storageListener = window.addEventListener('storage', function(e) {
        observer.next(e);
      });

      return () => window.removeEventListener('storage', storageListener);
    });
  }

  localStorageSetItem() {
    let random = Math.random()
    localStorage.setItem('test', JSON.stringify(random));
    if (random > 0.75) {

    }
  }

  get currentStepName() {
    return this.steps[this.currentStepIndex].name;
  }

  get currentForm() {
    return this.steps[this.currentStepIndex].form;
  }

  ngOnDestroy() {
    this.webSocketService.closeConnection();
  }
}
