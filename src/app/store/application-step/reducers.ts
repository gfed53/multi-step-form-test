import { createReducer, on } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import * as actions from './actions';
import { IApplicationStepState } from 'src/app/models/application-step-state';
import { ApplicationStep } from 'src/app/models/application-step';

export const adapter: EntityAdapter<ApplicationStep> = createEntityAdapter<ApplicationStep>({
  // selectId: ,
});

export const initialState: EntityState<ApplicationStep> = adapter.getInitialState({
  // additional entity state properties here
});

const _applicationStepReducer = createReducer(initialState,
  
);

export function reducer(state, action) {
  return _applicationStepReducer(state, action);
}
