import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { switchMap, map, tap, catchError, mergeMap } from 'rxjs/operators';
import { of } from 'rxjs';
import * as actions from './actions';

@Injectable()
export class ApplicationStepEffects {
  constructor(
    private actions$: Actions,
    ) {}

}
