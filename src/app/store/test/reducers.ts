import { createReducer, on } from '@ngrx/store';

import { ILeadState } from '../../models/lead-state';

import * as actions from './actions';

export const initialState: any = {
  test: null,
  callState: 'INIT'
};

const _testReducer = createReducer(initialState,
  on(actions.testWebSocketConnected, (state) => {
    return { ...state, callState: 'CONNECTED' };
  }),
  on(actions.testSetSocketValues, (state, { payload }) => {
    return { ...state, test: payload };
  }),
);

export function reducer(state, action) {
  return _testReducer(state, action);
}
