import { createAction, props } from '@ngrx/store';

/**
 * Effects
*/

/**
 * When this effect is fired, we return a continuous
 * stream of data returned to us from the websocket.
 * So we map each data to an action that sets the
 * data in our NgRx store.
 * This would mean that this effect would trigger a
 * continuous stream of actions until the websocket
 * connection is closed.
*/
export const testConnectWebSocket = createAction(
  '[Test] Connect WebSocket',
  props<{ url?: string }>()
);

/**
 * Basic Actions
*/

export const testWebSocketConnected = createAction(
  '[Test] WebSocket Connected'
);


/**
 * This is more of a notifier within NgRx than anything else.
 * It doesn't do anything with the sent values.
*/
export const testSendSocketValues = createAction(
  '[Test] Send Socket Values',
  // props<{ payload: any }>()
);

/**
 * This action is what actually would update the
 * store. It's dispatched based on values that come in
 * from the websocket.
*/
export const testSetSocketValues = createAction(
  '[Test] Set Socket Values',
  props<{ payload: any }>()
);

export const testSetSocketError = createAction(
  '[Test] Set Socket Error',
  props<{ payload: any }>()
);

/**
 * This is more of a notifier within NgRx than anything else.
*/
export const testCloseWebSocket = createAction(
  '[Test] Close WebSocket',
);
