import { createAction, props } from '@ngrx/store';
import { ILead } from 'src/app/models/lead';

/**
 * Effects
*/
export const leadPatchStep = createAction(
  '[Lead] PATCH Step',
  props<{ payload: any }>()
);

export const leadPatchSingleValue = createAction(
  '[Lead] PATCH Single Value',
  props<{ payload: any }>()
);

/**
 * 
*/
export const leadGetLead = createAction(
  '[Lead] GET Lead',
  props<{ id: number }>()
);

/**
 * Create a new lead from scratch right from Angular, via the
 * LeadService method. This would run if we don't have anything stored
 * in our backend.
*/
export const leadPostLead = createAction(
  '[Lead] POST Lead'
);

/**
 * Basic Actions
*/

export const leadSetValues = createAction(
  '[Lead] Set Values',
  props<{ payload: any }>()
);

/**
 *
*/
export const leadSetError = createAction(
  '[Lead] Set Error',
  props<{ error: any }>()
);

/**
 *
*/
export const leadSetNextStep = createAction(
  '[Lead] Set Next Step',
);

/**
 *
*/
export const leadSetPreviousStep = createAction(
  '[Lead] Set Previous Step',
);
