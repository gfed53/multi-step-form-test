import { createSelector, MemoizedSelector, createFeatureSelector } from '@ngrx/store';
import { ILeadState } from 'src/app/models/lead-state';
import { ILead } from 'src/app/models/lead';

const selectLead = createFeatureSelector<any, ILeadState>('lead');



export const selectLeadData: MemoizedSelector<any, ILead> = createSelector(
  selectLead,
  (lead: ILeadState) => lead.data
);

export const selectLeadStepIndex: MemoizedSelector<any, number> = createSelector(
  selectLead,
  (lead: ILeadState) => lead.stepIndex
);
