import { createReducer, on } from '@ngrx/store';

import { ILeadState } from '../../models/lead-state';

import * as actions from './actions';

export const initialState: ILeadState = {
  data: {
    // CONTACT
    first_name: null,
    last_name: null,
    street_address: null,
    city: null,
    state: null,
    zip: null,

    // PERSONAL
    dob: null,
    main_phone: null,
    second_phone: null,
    // ssn wouldn't live on the store
    annual_income: null,

    // EMPLOYMENT
    employment_status: null,
    length_of_employment: null,

    // HOUSING
    housing_type: null,
    housing_payment: null,

    // BANKING
    bank_accounts: null,

    // TERMS
    all_terms_agree: null,
  },
  stepIndex: 0,
  callState: 'init'
};

const _leadReducer = createReducer(initialState,
  on(actions.leadSetValues, (state, { payload }) => {
    return { ...state, data: { ...state.data, ...payload } };
  }),
  on(actions.leadSetNextStep, (state) => {
    return { ...state, stepIndex: state.stepIndex + 1 };
  }),
  on(actions.leadSetPreviousStep, (state) => {
    return { ...state, stepIndex: state.stepIndex - 1 };
  }),
);

export function reducer(state, action) {
  return _leadReducer(state, action);
}
