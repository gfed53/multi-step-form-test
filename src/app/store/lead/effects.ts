import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { switchMap, map, tap, catchError, mergeMap } from 'rxjs/operators';
import { of } from 'rxjs';
import * as actions from './actions';
import { LeadService } from 'src/app/services/lead.service';

@Injectable()
export class LeadEffects {
  constructor(
    private actions$: Actions,
    private leadService: LeadService
    ) {}

  leadPatchValues$ = createEffect(
    () => this.actions$.pipe(
      ofType(actions.leadPatchStep, actions.leadPatchSingleValue),
      tap((a) => {
        // console.log('effect firing', a);
      }),
      switchMap(({ type, payload }) => of(payload).pipe(
      // switchMap(({ type, payload }) => this.leadService.patchLead(1234, payload).pipe(
        /**
         * In the real world, this 'of' observable would be an API call, where the
         * response would be saved lead values, which we would update in our store.
         * There are some values that we don't want to keep in the store (like SSN),
         * so we can simulate getting a different response back for now.
         *
         * TODO: PATCH method isn't implemented in the in-memory-web-api, so we can't use
         * patchLead method just yet. Currently trying to create a simple backend to use
         * instead of having to rely on this.
        */
        map(res => {
          const updated = { ...res };
          const omittedFields = [
            'ssn',
            'has_checking', 'has_savings', 'has_retirement', 'has_investments',
          ];
          omittedFields.forEach(field => {
            // if (updated[field]) {
              delete updated[field];
            // }
          });
          return updated;
        }),
        switchMap((res) => {
          console.log('leadPatchValues res', res);

          /**
           * When we PATCH the whole form, we also want to
           * proceed to the next page
          */
          if (type === actions.leadPatchStep.type) {
            return of(
              actions.leadSetValues({ payload: res }),
              actions.leadSetNextStep()
            )
          } else {
            /**
             * This would be just for PATCHing individual fields
            */
            return of(actions.leadSetValues({ payload: res }));
          }
        }),
        catchError((err) => {
          console.log('leadPatchValues err', err);
          return of(actions.leadSetError({ error: err }));
        }),
      ))
    )
  );

  leadGetLead$ = createEffect(
    () => this.actions$.pipe(
      ofType(actions.leadGetLead),
      tap((a) => {
        // console.log('effect firing', a);
      }),
      switchMap(({ type, id }) => this.leadService.getLead(id).pipe(
        switchMap((res) => {
          console.log('leadGetLead res', res);

          /**
           *
          */
          return of(
            actions.leadSetValues({ payload: res }),
          )
        }),
        catchError((err) => {
          // console.log('err', err);
          // Create a lead from scratch if we don't have one saved in backend.
          return err.status === 404 ? of(actions.leadPostLead()) : of(actions.leadSetError({ error: err }));
          return of(actions.leadSetError({ error: err }));
        }),
      ))
    )
  );

  leadPostLead$ = createEffect(
    () => this.actions$.pipe(
      ofType(actions.leadPostLead),
      tap((a) => {
        // console.log('leadPostLead effect firing', a);
      }),
      switchMap(() => this.leadService.createLead().pipe(
        /**
         * In the real world, the
         * response would be saved lead values, which we would update in our store.
         * There are some values that we don't want to keep in the store (like SSN),
         * so we can simulate getting a different response back for now.
        */
        map(res => {
          // console.log('res', res);
          const updated = { ...res };
          const omittedFields = [
            'ssn',
            'has_checking', 'has_savings', 'has_retirement', 'has_investments',
          ];
          omittedFields.forEach(field => {
            // if (updated[field]) {
              delete updated[field];
            // }
          });
          return updated;
        }),
        switchMap((res) => {
          // console.log('res', res);

          /**
           *
          */
          return of(
            actions.leadSetValues({ payload: res }),
          )
        }),
        catchError((err) => {
          console.log('err', err);
          return of(actions.leadSetError({ error: err }));
        }),
      ))
    )
  );
}
