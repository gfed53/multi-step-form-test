import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-generic-questions',
  templateUrl: './generic-questions.component.html',
  styleUrls: ['./generic-questions.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class GenericQuestionsComponent implements OnInit {

  @Input() step: string;
  @Input() form: FormGroup;


  constructor() { }

  ngOnInit(): void {
    // console.log(this.form);
    // console.log(this.fields);
  }

  get fields() {
    return this.form.controls;
  }

  employmentStatusBlur(event) {
    if (event.target.value === '2') {
      // '4' is N/A. We default to this value when Unemployed is selected
      this.fields['length_of_employment'].setValue('4');
    } else {
      if (this.fields['length_of_employment'].value === '4') {
        this.fields['length_of_employment'].setValue(null);
      }
    }
  }

  housingTypeBlur(event) {
    if (event.target.value === 'own') {
      // Just force the value to 0 if the user selects 'own'
      // Implies that payment is irrelevant for this choice.
      this.fields['housing_payment'].setValue(0);
    }
  }

  // Just for the form, we have these 4 fields, but what we send to the backend is just a string interpretation
  // of what choices were selected.
  bankAccountFieldsBlur(event) {
    const bankAccountFieldChoices = ['has_checking', 'has_savings', 'has_retirement', 'has_investments']
      .map(fieldName => this.fields[fieldName].value)
      .map(fieldValue => fieldValue ? 'Y' : 'N')
      .join('');

    // console.log('bankAccountFieldChoices', bankAccountFieldChoices);
    this.fields['bank_accounts'].patchValue(bankAccountFieldChoices);
  }

}
