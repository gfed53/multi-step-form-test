import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GenericQuestionsComponent } from './generic-questions.component';

describe('GenericQuestionsComponent', () => {
  let component: GenericQuestionsComponent;
  let fixture: ComponentFixture<GenericQuestionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GenericQuestionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenericQuestionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
