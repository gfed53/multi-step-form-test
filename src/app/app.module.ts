import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { GenericQuestionsComponent } from './generic-questions/generic-questions.component';
import { fromLead, fromTest, fromApplicationStep } from './store/reducers';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { storeFreeze } from 'ngrx-store-freeze';
import { environment } from '../environments/environment';
import { LeadEffects } from './store/lead/effects';
import { InMemoryLeadsService } from './services/in-memory-leads.service';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { TestEffects } from './store/test/effects';

@NgModule({
  declarations: [
    AppComponent,
    GenericQuestionsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    HttpClientInMemoryWebApiModule.forRoot(InMemoryLeadsService),
    ReactiveFormsModule,
    StoreModule.forRoot({
      applicationStep: fromApplicationStep,
      lead: fromLead,
      test: fromTest
    }, {
      metaReducers: !environment.production ? [storeFreeze] : []
    }),
    EffectsModule.forRoot([
      LeadEffects,
      TestEffects
    ]),
    StoreDevtoolsModule.instrument({ maxAge: 25, logOnly: environment.production })
  ],
  providers: [
    InMemoryLeadsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
