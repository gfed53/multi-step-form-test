import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ILead } from '../models/lead';

@Injectable({
  providedIn: 'root'
})
export class LeadService {

  constructor(
    private http: HttpClient
  ) { }

  getLeads() {
    return this.http.get('api/leads');
  }

  getLead(id) {
    return this.http.get(`api/leads/${id}`);
  }

  createLead() {
    const lead: ILead = {

      id: 1234,

      // CONTACT
      first_name: null,
      last_name: null,
      street_address: null,
      city: null,
      state: null,
      zip: null,

      // PERSONAL
      dob: null,
      main_phone: null,
      second_phone: null,
      ssn: null,
      annual_income: null,

      // EMPLOYMENT
      employment_status: null,
      length_of_employment: null,

      // HOUSING
      housing_type: null,
      housing_payment: null,

      // BANKING
      bank_accounts: null,

      // TERMS
      all_terms_agree: null,
    };

    return this.http.post(`api/leads/`, lead);
  }

  patchLead(id, leadData) {
    return this.http.patch(`api/leads/${id}`, leadData);
  }

}
