import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';
import { ILead } from '../models/lead';

@Injectable({
  providedIn: 'root'
})
export class InMemoryLeadsService implements InMemoryDbService {

  constructor() { }

  createDb() {
    let leads: ILead[] = [];

    return { leads };
  }
}
