import { ValidatorFn, AbstractControl } from '@angular/forms';

/**
 * Factory that returns a new regex-based validator
*/
export function regexValidator(type: string, re: RegExp): ValidatorFn {
  return (control: AbstractControl): {[key: string]: any} | null => {
    const match = re.test(control.value);
    return !match ? { [type]: {
      value: control.value,
      errorString: `Invalid ${type}` // simple for now
    } } : null;
  };
}
