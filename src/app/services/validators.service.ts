import { Injectable } from '@angular/core';
import { regexValidator } from './validator-helpers';
import { AbstractControl, FormGroup, ValidationErrors, Form, FormControl } from '@angular/forms';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';

import * as moment from 'moment';
import { selectLeadData } from '../store/lead/selectors';
import { take, map } from 'rxjs/operators';
import { ILead } from '../models/lead';

@Injectable({
  providedIn: 'root'
})
export class ValidatorsService {

  stateZipMap = {
    NY: [
      '10994',
      '10001'
    ],
    CA: [
      '90210'
    ],
    UT: [
      '84101',
      '84102'
    ]
  };

  constructor(
    private store: Store<any>
  ) { }

  phoneRegex() {
    return regexValidator('phone', /^\d{10}$/);
  }

  ssnRegex() {
    return regexValidator('ssn', /^\d{9}$/);
  }

  stateZipMismatch(control: FormGroup): ValidationErrors | null {
    const state = control.get('state');
    const zip = control.get('zip');
    return this.stateZipMap[state.value] && !this.stateZipMap[state.value].includes(zip.value) ? {'stateZipMismatch': {value: true}} : null;
  };

  incomeLowForDob(control: FormGroup): ValidationErrors | null {
    const dob = control.get('dob');
    const annual_income = control.get('annual_income');

    // debugger;

    const dobDate = moment(dob.value);
    const compareDate = moment('1970-01-01');

    const olderThanCompareDate = dobDate < compareDate;

    return olderThanCompareDate && parseInt(annual_income.value, 10) < 50000 ? {'incomeLowForDob': { value : true }} : null;
  }

  incomeLowForStateOfUtah(control: AbstractControl):
    Promise<ValidationErrors | null> | Observable<ValidationErrors | null> {

      // We're checking changes for either the state or income inputs?
      // Or just income?
      /**
       * Depending on which field we check, we check the other field on the lead.
       * I.e. on a change in state form input, we check that value against income that
       * we may have in the lead.
       *
       * For now, maybe just focus on checking the income field
      */

      // debugger;

      return this.store.pipe(
        select(selectLeadData),
        // skipWhile?
        take(1),
        map((leadData: ILead) => {
          if (parseInt(control.value) < 30000 && leadData.state === 'UT') {
            return {'incomeLowForStateOfUtah': { value: true }}
          }

          return null;
        })
      );
    }


}
