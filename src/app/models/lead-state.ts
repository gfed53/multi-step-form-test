import { ILead } from './lead';

export interface ILeadState {

  data: ILead;
  stepIndex?: number;
  callState?: string;
}
