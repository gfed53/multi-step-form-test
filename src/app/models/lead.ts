export interface ILead {
  // For in memory API to work
  id?: number,

  // CONTACT
  first_name?: string;
  last_name?: string;
  street_address?: string;
  city?: string;
  state?: string;
  zip?: string;

  // PERSONAL
  dob?: string;
  main_phone?: string;
  second_phone?: string;
  /**
   * ssn wouldn't live on the store, so this would
   * only be passed when we set to the backend.
   */
  ssn?: string;
  annual_income?: number;

  // EMPLOYMENT
  employment_status?: string;
  length_of_employment?: string;

  // HOUSING
  housing_type: string;
  housing_payment: number;

  // BANKING
  bank_accounts: string;

  // TERMS
  all_terms_agree: boolean;

}
