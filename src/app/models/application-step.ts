export interface ApplicationStep {
  name: string;
  isAllowed: boolean
}